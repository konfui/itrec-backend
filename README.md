# QIR Backend

[![pipeline status](https://gitlab.com/konfui/qir-backend/badges/master/pipeline.svg)](https://gitlab.com/konfui/qir-backend/commits/master)
[![coverage report](https://gitlab.com/konfui/qir-backend/badges/master/coverage.svg)](https://gitlab.com/konfui/qir-backend/commits/master)

This project provides backend that serves data for QIR Mobile application. It
uses Google Sheets API and Cloud Functions on Firebase. Google Sheets serves
as data repository that will be used by events administrator to provide data
that shall be displayed in the mobile app. The mobile app then fetches the
data from the Google Sheets using API endpoint implemented as Cloud Functions
on Firebase.

## Getting Started

You will need:

- Node.js v8
  - (Optional, but recommended) Use `nvm` (Node Version Manager) to allow
    multiple Node.js version in the same machine
- `yarn` package manager
  - Install globally using `npm`: `npm install -g yarn`

Install required dependencies using `yarn`:

```bash
yarn global add firebase-tools
```

Then change into `functions` directory and install the dependencies:

```bash
cd functions
yarn install
```

## Maintainers

- [Daya Adianto](https://gitlab.com/addianto)

## License

Copyright (C) 2019 Pusilkom Universitas Indonesia.

This project is licensed under [GPLv3](LICENSE).
