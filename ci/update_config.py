#!/usr/bin/env python
import json
import sys
from subprocess import call

with open(sys.argv[1], 'r') as config_file:
    config_tree = json.load(config_file)

    for feature in config_tree:
        for config_key in config_tree[feature]:
            config_value = config_tree[feature][config_key]
            config_string = f'{feature}.{config_key}={config_value}'
            call(['firebase', f'functions:config:set', config_string], shell=True)
