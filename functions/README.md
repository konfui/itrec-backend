# QIR Backend - Cloud Functions

## Endpoints

The root URL for all endpoints: `https://asia-northeast1-qir-ftui.cloudfunctions.net`.
All requests must have `Content-Type: application/vnd.api+json` in the header.

- `GET /getEvents`
  - Parameters: N/A
  - Examples:
    - `GET /getEvents`
      - Retrieves all event rundown data.
- `GET /publications`
  - Parameters:
    - `id`: unique identifier of a publication
    - `paperCode`: unique identifier of a publication retrieved from EasyChair
    - `symposium`: name of a symposium (poster session also considered as a symposium)
    - `title`: title of a publication
    - `authors`: author of a publication
  - Examples:
    - `GET /publications?id=1`
      - Retrieves an array of publication object containing single object of publication
      with matching ID.
    - `GET /publications?authors=giorno`
      - Retrieves an array of publication objects that contains all publications with
      name of author matches with `giorno`.
- `GET /sessions`
  - Parameters:
    - `title`: title of a publication
  - Examples:
    - `GET /sessions`
      - Retrieves an array of all session (symposium) objects.
    - `GET /sessions?title=TITLE`
      - Retrieves an array of session objects that has title contains all sessions with
      title matching with `TITLE`.
- `GET /speakers`
  - Parameters:
    - `id`: unique identifier of a speaker
    - `name`: name of a speaker
    - `type`: type of a speaker (e.g. `invited`, `keynote`)
  - Examples:
    - `GET /speakers?name=Giorno Giovanna`
      - Retrieves a list of single speaker object whose name is Giorno Giovanna
    - `GET /speakers?type=keynote`
      - Retrieves a list of speakers that invited to deliver keynote speeches
- `GET /authors`
  - Parameters:
    - `author`: name of an author
  - Examples:
    - `GET /authors`
      - Retrieves a list of unique author objects
    - `GET /authors?fullName=Giorno Giovanna`
      - Retrieves an object of author whose full name is Giorno Giovanna
- `GET /search`
  - Parameters:
    - `author`: search query by the name of an author
    - `keyword`: search query by one or more keywords
    - `title`: search query by title
  - Examples:
    - `GET /search?author=Gio`
      - Retrieves a list of authors whose name prefixed with Gio
    - `GET /search?keyword=stand`
      - Retrieves a list of publications (papers) whose keyword contains a word
      named `stand`
    - `GET /search?title=framework`
      - Retrieves a list of publication (papers) whose title contains a word
      named `framework`
