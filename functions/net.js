const HttpStatus = require('http-status-codes');
const JSON_TYPE = 'application/vnd.api+json';
const META_OBJECT = {
    "copyright": "Copyright (c) 2019 Pusilkom Universitas Indonesia",
};

exports.sendMethodNotAllowedError = (request, response) => {
    response.status(HttpStatus.METHOD_NOT_ALLOWED).type(JSON_TYPE).send({
        errors: [
            {
                status: `${HttpStatus.METHOD_NOT_ALLOWED}`,
                title: HttpStatus.getStatusText(HttpStatus.METHOD_NOT_ALLOWED),
                detail: 'Invalid content type in the request header',
                source: {
                    parameter: `Content-Type: ${request.headers['content-type']}`,
                },
            },
        ],
    });
};

exports.sendBadGatewayError = (response) => {
    response.status(HttpStatus.BAD_GATEWAY).type(JSON_TYPE).send({
        errors: [
            {
                status: `${HttpStatus.BAD_GATEWAY}`,
                title: HttpStatus.getStatusText(HttpStatus.BAD_GATEWAY),
                detail: 'There was a problem communicating with Google Sheets API',
            }
        ]
    });
};

exports.sendResults = (response, results) => {
    response.status(HttpStatus.OK).type(JSON_TYPE).send({
        data: results,
        meta: META_OBJECT,
    });
};

exports.JSON_TYPE = JSON_TYPE;