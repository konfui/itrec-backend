exports.search = (query, attribute, data) => {
    if (data.some(element => !Object.keys(element).includes(attribute))) {
        return [];
    }

    return data.map(stringifyAttributes)
               .filter(element => match(element[attribute], query));
};

const stringifyAttributes = (obj) => {
    const result = {};

    Object.keys(obj).forEach(attribute => {
        result[attribute] = typeof obj[attribute] === 'string' ? obj[attribute] :
            String(obj[attribute]);
    });

    return result;
};

const match = (element, query) => {
    const elementString = element.toLowerCase();
    const queryString = query.toLowerCase();

    return elementString.includes(queryString);
};