// SPDX-License-Identifier: GPL-3.0-or-later
'use strict'
const comparator = {
    authorByFullname: (a, b) => {
        if (a.fullName < b.fullName) {
            return -1;
        } else if (a.fullName > b.fullName) {
            return 1;
        } else {
            return 0;
        }
    },
    authorByName: (a, b) => {
        if (a.name < b.name) {
            return -1;
        } else if (a.name > b.name) {
            return 1;
        } else {
            return 0;
        }
    },
    publicationByDate: (a, b) => {
        if (a.date < b.date) {
            return -2;
        } else if (a.date > b.date) {
            return 2;
        } else if (a.time < b.time) {
            return -1;
        } else if (a.time > b.time) {
            return 1;
        } else {
            return 0;
        }
    },
};

exports.comparator = comparator;
