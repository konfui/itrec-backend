const {describe} = require('mocha');
const {expect} = require('chai');
const HttpStatus = require('http-status-codes');
const {FakeRequest, FakeResponse} = require('./fakes');
const net = require('../net');

describe('Net functions', () => {
    context('sendMethodNotAllowedError()', () => {
        it('sends error correctly', () => {
            const request = FakeRequest('application/vnd.api+xml');
            const response = FakeResponse();

            net.sendMethodNotAllowedError(request, response);

            expect(response._status).to.equal(HttpStatus.METHOD_NOT_ALLOWED);
            verifyErrorFormat(expect, response);
        });
    });
    context('sendBadGatewayError()', () => {
        it('sends error correctly', () => {
            const response = FakeResponse();

            net.sendBadGatewayError(response);

            expect(response._status).to.equal(HttpStatus.BAD_GATEWAY);
            verifyErrorFormat(expect, response);
        });
    });
    context('sendResults()', () => {
        const response = FakeResponse();

        net.sendResults(response, [{foo: 'bar'}, {foo: 'baz'}]);

        expect(response._status).to.equal(HttpStatus.OK);
        expect(response._type).to.equal('application/vnd.api+json');
        expect(response._send.data.length).to.not.equal(0);
    });
});

const verifyErrorFormat = (expect, response) => {
    expect(response._type).to.equal('application/vnd.api+json');
    expect(response._send.errors.length).to.not.equal(0);
}
