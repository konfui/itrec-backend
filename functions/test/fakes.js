exports.FakeRequest = function (header) {
    return {
        headers: {
            'content-type': header,
        }
    };
};

exports.FakeResponse = function () {
    return {
        status: function (arg) {
            this._status = arg;
            return this;
        },
        type: function(arg) {
            this._type = arg;
            return this;
        },
        send: function(arg) {
            this._send = arg;
            return this;
        }
    }
};