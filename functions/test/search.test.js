const {describe} = require('mocha');
const {expect} = require('chai');
const search = require('../lib/search').search;

describe('Search-related functions', () => {
    context('search()', () => {
        const fakeData = [
            {
                foo: 1,
                bar: 2,
                baz: 'three',
                boz: 'four',
            },
            {
                foo: 5,
                bar: 6,
                baz: 'seven',
                boz: 'eight',
            },
            {
                foo: 9,
                bar: 10,
                baz: 'eleven',
                boz: 'twelve',
            },
            {
                foo: 11,
                bar: 22,
                baz: 'three',
                boz: 'four',
            },
            {
                foo: 55,
                bar: 66,
                baz: 'seven',
                boz: 'eight',
            },
            {
                foo: 99,
                bar: 10,
                baz: 'eleven',
                boz: 'twelve',
            },
        ];

        it('returns a collection of found objects when searching on attribute with integer value', () => {
            const results = search('10', 'bar', fakeData);

            expect(results.length).to.equal(2);
        });

        it('returns a collection of found objects when searching on attribute with string value', () => {
            const results = search('three', 'baz', fakeData);

            expect(results.length).to.equal(2);
        });

        it('returns an empty collection when searching on unexisting attribute', () => {
            const results = search('10', 'barista', fakeData);

            expect(results.length).to.equal(0);
        });

        it('returns an empty collection when searching for unexisting data', () => {
            const results = search('twenty', 'boz', fakeData);

            expect(results.length).to.equal(0);
        });
    });
});