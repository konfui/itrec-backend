const CONTENT_TYPE_HEADER = 'content-type';
const DEFAULT_REGION = 'asia-northeast1';

exports.isValidRequest = (request, expectedContentType) => {
    if (request.headers === undefined) return false;

    const contentType = (request.headers.hasOwnProperty(CONTENT_TYPE_HEADER)) ?
        request.headers[CONTENT_TYPE_HEADER] : '';

    return contentType.startsWith(expectedContentType);
};

exports.createObject = (attributes, row) => {
    const event = {};

    attributes.forEach((attribute, index) => {
        event[attribute] = index < row.length ? row[index] : '';
    });

    return event;
};

exports.sheetsToObjects = (sheets) => {
    const attributes = sheets[0];
    const rows = sheets.slice(1);

    return rows.map((row) => this.createObject(attributes, row));
};

exports.extractAuthors = (data) => {
    const results = [];

    data.forEach((entry) => {
        const authors = entry.authors.split(',')
            .map((author) => author.trim());
        authors.forEach((author) => {
            if (author.includes('and')) {
                const cleanedAuthors = author.split(/and /)
                    .map((author) => author.trim());

                cleanedAuthors.forEach((author) => {
                    if (!results.includes(author) && author !== '') {
                        results.push(author);
                    }
                });
            } else {
                if (!results.includes(author)) {
                    results.push(author);
                }
            }
        });
    });

    results.sort();

    return results
};

exports.filterUnique = (data, attributes) => {
    if (attributes === undefined) {
        return [...new Set(data)];
    }

    const uniqueChecker = new Set();
    const uniqueIndex = [];

    data.forEach((entry, index) => {
        let key = '';
        attributes.forEach((attribute) => {
            key = key + entry[attribute];
        });

        if (!uniqueChecker.has(key)) {
            uniqueIndex.push(index);
            uniqueChecker.add(key);
        }
    });

    return uniqueIndex.map((index) => data[index]);
};

exports.DEFAULT_REGION = DEFAULT_REGION;
