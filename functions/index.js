// SPDX-License-Identifier: GPL-3.0-or-later
const functions = require('firebase-functions');
const utils = require('./utils');
const {comparator} = require('./src/collection');
const {getValues} = require('./src/sheets');
const net = require('./net');

exports.getEvents = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        const {key, id, range} = functions.config().events;

        getValues(key, id, range).then((data) => {
            const results = utils.sheetsToObjects(data);
            net.sendResults(response, results);
            return results;
        }).catch((reason) => {
            console.error(reason);
            net.sendBadGatewayError(response);
        });
    });

exports.publications = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        const {key, id, range} = functions.config().publications;

        getValues(key, id, range).then((data) => {
            let results = utils.sheetsToObjects(data);

            if (Object.getOwnPropertyNames(request.query).length > 0) {
                results = filterResults(results, request.query, [
                    'track', 'title', 'authors', 'keywords', 'decision',
                    'abstract', 'organization', 'location', 'date', 'time', 'chair',
                ]);
            }

            net.sendResults(response, results.sort(comparator.publicationByDate));
            return results;
        }).catch((reason) => {
            console.error(reason);
            net.sendBadGatewayError(response);
        });
    });

exports.sessions = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        const {key, id, range} = functions.config().sessions;

        getValues(key, id, range).then((data) => {
            let results = utils.sheetsToObjects(data);

            if (Object.getOwnPropertyNames(request.query).length > 0) {
                results = filterResults(results, request.query, [
                    'id', 'symposium', 'title', 'isPoster',
                ]);
            }

            net.sendResults(response, results);
            return results;
        }).catch((reason) => {
            console.error(reason);
            net.sendBadGatewayError(response);
        });
    });

exports.speakers = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        const {key, id, range} = functions.config().speakers;

        getValues(key, id, range).then((data) => {
            let results = utils.sheetsToObjects(data);

            if (Object.getOwnPropertyNames(request.query).length > 0) {
                results = filterResults(results, request.query, [
                    'id', 'name', 'type'
                ]);
            }

            const sortedResults = results.sort(comparator.authorByName);

            net.sendResults(response, sortedResults);
            return results;
        }).catch((reason) => {
            console.error(reason);
            net.sendBadGatewayError(response);
        });
    });

exports.authors = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        const {key, id, range} = functions.config().authors;

        getValues(key, id, range).then((data) => {
            let results = utils.sheetsToObjects(data);

            if (Object.getOwnPropertyNames(request.query).length > 0) {
                results = filterResults(results, request.query, [
                    'submissionId', 'email', 'country', 'organization',
                    'personId', 'fullName',
                ]);
            }

            const uniqueResults = utils.filterUnique(results, ['fullName', 'email']);
            const sortedResults = uniqueResults.sort(comparator.authorByFullname)
                                               .filter((e) => e.fullName !== ' ');

            net.sendResults(response, sortedResults);
            return sortedResults;
        }).catch((reason) => {
            console.error(reason);
            net.sendBadGatewayError(response);
        });
    });

exports.search = functions.region(utils.DEFAULT_REGION).https
    .onRequest((request, response) => {
        if (!utils.isValidRequest(request, net.JSON_TYPE)) {
            net.sendMethodNotAllowedError(request, response);
            return;
        }

        if (Object.getOwnPropertyNames(request.query).length > 0) {
            let results = [];
            if ('author' in request.query) {
                searchAuthor(request.query['author'].toLowerCase()).then(authors => {
                    results = authors.sort(comparator.authorByFullname)
                                     .filter((e) => e.fullName !== ' ');
                    net.sendResults(response, results);
                    return results;
                }).catch((error) => {
                    console.error(error);
                    net.sendBadGatewayError(response);
                });
            }

            if ('keyword' in request.query) {
                searchKeyword(request.query['keyword'].toLowerCase()).then(publications => {
                    results = publications.sort(comparator.publicationByDate);
                    net.sendResults(response, results);
                    return results;
                }).catch((error) => {
                    console.error(error);
                    net.sendBadGatewayError(response);
                });
            }

            if ('title' in request.query) {
                searchTitle(request.query['title'].toLowerCase()).then(publications => {
                    results = publications.sort(comparator.publicationByDate);
                    net.sendResults(response, results);
                    return results;
                }).catch((error) => {
                    console.error(error);
                    net.sendBadGatewayError(response);
                });
            }

            if ('room' in request.query) {
                searchRoom(request.query['room'].toLowerCase()).then(publications => {
                    results = publications;
                    net.sendResults(response, results);
                    return results;
                }).catch((error) => {
                    console.error(error);
                    net.sendBadGatewayError(response);
                });
            }
            // TODO Handle case when the request is empty
        }
    });

// TODO Handle complex query, e.g. multiple keywords
const filterResults = (results, query, params) => {
    let filtered = results;
    console.log('filterResults:', query);

    params.forEach((p) => {
        if (p in query) {
            filtered = filtered.filter(entry => match(entry[p], query[p]));
        }
    });

    console.log(`filterResults: # of results: ${filtered.length}`);

    return filtered;
};

const match = (value, query) => {
    if (value === undefined) return false;
    return value.toLowerCase().includes(query.toLowerCase());
};

const searchAuthor = (query) => new Promise((resolve, reject) => {
    const {key, id, range} = functions.config().authors;

    const results = getValues(key, id, range).then((data) => {
        const authors = utils.sheetsToObjects(data);
        const filteredAuthors = authors.filter(author => {
            const fullName = author.fullName;
            return fullName.toLowerCase().includes(query);
        });

        return utils.filterUnique(filteredAuthors, ['fullName', 'email'])
                    .sort(comparator.authorByFullname)
                    .filter((e) => e.fullName !== ' ');
    }).catch((error) => {
        console.error(error);
        reject(error);
    });

    resolve(results);
});

const searchKeyword = (query) => new Promise((resolve, reject) => {
    const {key, id, range} = functions.config().publications;

    const results = getValues(key, id, range).then((data) => {
        const publications = utils.sheetsToObjects(data);

        return publications.filter(publication => {
            const keywords = publication.keywords;
            return keywords.toLowerCase().includes(query);
        });
    }).catch((error) => {
        console.error(error);
        reject(error);
    });

    resolve(results);
});

const searchTitle = (query) => new Promise((resolve, reject) => {
    const {key, id, range} = functions.config().publications;

    const results = getValues(key, id, range).then((data) => {
        const publications = utils.sheetsToObjects(data);

        return publications.filter(publication => {
            const title = publication.title;
            return title.toLowerCase().includes(query);
        });
    }).catch((error) => {
        console.error(error);
        reject(error);
    });

    resolve(results);
});

const searchRoom = (query) => new Promise((resolve, reject) => {
    const {key, id, range} = functions.config().publications;

    const results = getValues(key, id, range).then((data) => {
        const publications = utils.sheetsToObjects(data);

        return publications.filter(publication => {
            const room = publication.location;
            return room.toLowerCase().includes(query);
        });
    }).catch((error) => {
        console.error(error);
        reject(error);
    });

    resolve(results);
});
